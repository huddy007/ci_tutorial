package ci_practical;

public class Person {
	
	private String firstName;
	private String lastName;
	
	public Person (String firstName, String lastName) {
		this.firstName = firstName;
		this.lastName = lastName;
	}
	
	public String getFirstName() {
		return firstName;
	}
	
	public String getLastName() {
		return lastName;
	}
	
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	
	public void setLastName(String lastName) {
		System.out.println("Not Yet Implemented...");
	}
}