package ci_practical;

import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.Test;
	
public class PersonTest {

		private Person person;
		
		@Test
		void testSetFirstName() {
			person = new Person("John", "Smith");
			person.setFirstName("Rodion");
			assertEquals("Rodion", person.getFirstName() );
		}
		
		@Test
		void testSetLastName() {
			person = new Person ("John", "Smith");
			person.setLastName("Raskolnikov");
			assertEquals("Raskolnikov", person.getLastName() );
		}
	
}